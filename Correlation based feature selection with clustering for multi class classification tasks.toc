\babel@toc {english}{}
\contentsline {chapter}{List of Figures}{vii}{chapter*.2}% 
\contentsline {chapter}{List of Tables}{viii}{chapter*.3}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Objectives of the Thesis}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Organization of the Thesis}{2}{section.1.3}% 
\contentsline {chapter}{\numberline {2}Related Work}{4}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Proposed Method}{5}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Data Preprocessing}{5}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Missing value Handling By KNN Imputation}{6}{subsection.3.1.1}% 
\contentsline {section}{\numberline {3.2}Correlation Feature Grouping Method}{6}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Find Covariance Matrices Calculation for Each Sample}{7}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Affinity Propagation Clustering}{8}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Random Features from Cluster Group}{10}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Co-relation base feature selection with Clustering}{10}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Decision Tree}{10}{subsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.1.1}Decision Tree (ID3 Algorithm)}{10}{subsubsection.3.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.3.1.2}Decision tree (CART)}{13}{subsubsection.3.3.1.2}% 
\contentsline {subsection}{\numberline {3.3.2}Random Forest}{13}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}AdaBoost Algorithm}{15}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Voting Algorithm(Svm, C4.5, Logistic Regression)}{17}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Logistic Regression}{17}{subsection.3.3.5}% 
\contentsline {subsection}{\numberline {3.3.6}Bagging Algorithm}{18}{subsection.3.3.6}% 
\contentsline {subsection}{\numberline {3.3.7}SVM algorithm}{19}{subsection.3.3.7}% 
\contentsline {chapter}{\numberline {4}Data sets And Comparison of Performance}{20}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Datasets}{20}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}HAPT (Smartphone-Based Recognition of Human Activities and Postural Transitions) Data Set}{20}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Gas sensors for home activity monitoring Data Set }{21}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}Weight Lifting Exercises monitored with Inertial Measurement Units Data Set }{22}{subsection.4.1.3}% 
\contentsline {subsection}{\numberline {4.1.4}SECOM Data Set }{22}{subsection.4.1.4}% 
\contentsline {section}{\numberline {4.2}Experimental Analysis}{23}{section.4.2}% 
\contentsline {chapter}{\numberline {5}Conclusions}{29}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Conclusions}{29}{section.5.1}% 
\contentsline {chapter}{Bibliography}{30}{chapter*.18}% 
\vspace {2em}
\vspace {2em}
